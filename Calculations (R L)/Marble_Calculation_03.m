%% Data
%--------------------------------------------
%Element wise multiplication .*

%cable
%--------------
dall = [0.3 0.45 0.5 0.75]* 1e-3;   %mm
Acable = pi .* (dall./2).^2;

%Coil Area
%------------
lcoil = 0.015;                      %mm
hcoil = 0.005;                      %mm
racoile = 0.010;                    %mm
dtotalcoli = 1.256e-6;              %mm
umcoile= 2* pi * racoile;           %mm
Acoil= pi * racoile^2;              %mm


%Values
%---------------
roh= 1.68*10^-8;                    %Ω*mm/m copper 20°C
mu0 = 4*pi* 10^-7; 


%% Calculation
%--------------------------------------------
% Kable Numbers Length
%----------------------
nl = floor(lcoil./dall);  %N

% Kable Numbers wide
%----------------------
nw = floor(hcoil./dall); %N

%Numbers of Turnes
%--------------------
turnsall = floor(nl.*nw); %turns

% without one Layer windings 
% (the additional 1 behind the variable is one layer less)
%--------------------
turnsall1 = turnsall - nl; %turns

%Length wire
%--------------------
l = umcoile.*turnsall;         %m
l1 = umcoile.*turnsall1;       %m 

% Ω per wire
%--------------------
Rall = roh*l./Acable;          %Ω 
Rall1 = roh.*l1./Acable;       %Ω

% mH per wire
%--------------------
Lall = ((mu0.*turnsall.^2*racoile^2*pi)/(lcoil+0.9*racoile));   %H 
Lall1 = ((mu0.*turnsall1.^2*racoile^2*pi)/(lcoil+0.9*racoile)); %H 


%% Plots
% mm^2 and N
windings = [turnsall; turnsall1];
figure (1)
title("Windings and Diamiter ");

plot (dall.*1000, turnsall,'-o',dall.*1000,turnsall1,'-o', 'LineWidth',2);
grid on
%axis([0.2*1e-6 0.8*1e-6 50 200])
leg =legend ('Max windings turns/N', 'minus one Layer turns/N');
set(leg,'FontSize',15)
title("Turns and Cable Diamiter","FontSize", 20)
xlabel("Diamiter/ mm",'FontSize', 20);
ylabel("Windings/ N",'FontSize', 20);



figure (2)
% mm^2 and L/R
Resistens = [Rall; Rall1];
Inductivity = [Lall; Lall1];
tiledlayout(2,1)
% Top plot
ax1 = nexttile;
grid on
plot(ax1,dall.*1000,Resistens, '-o','LineWidth',2)
legend ('Max windings R/Ω', 'minus one Layer R/Ω',"FontSize", 15);
title(ax1,'Area of the Cable and R',"FontSize", 15)
xlabel("Diamiter/ mm",'FontSize', 15);
ylabel(ax1,'R/ Ω','FontSize', 15)
grid on

% Bottom plot
ax2 = nexttile;
plot(ax2,dall.*1000,Inductivity.*1000, '-o','LineWidth',2)
legend ('Max windings L/mH','minus one Layer L/mH',"FontSize", 15);
title(ax2,'Area of the Cable and L',"FontSize", 15)
xlabel("Diamiter/ mm",'FontSize', 15);
ylabel(ax2,'L/ mH','FontSize', 15)
grid on

%% RL Calculations
%L= tau* R 
%L=U*t/I

Uosi= 10;       %V
tosi= 520*1-6;  %µs
Iosi= 3;        %A
Iosimax= 5.3;   %A


Losi = ((Uosi*tosi)/Iosi)*1000;

%Steady state
Rosi = Uosi/Iosimax;











