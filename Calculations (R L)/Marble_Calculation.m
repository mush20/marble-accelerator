%cable
%--------------
Acable1 = 0.3 * 1e-6;     %m^2
Acable2 = 0.45 * 1e-6;    %m^2
Acable3 = 0.5 * 1e-6;     %m^2
Acable4 = 0.75* 1e-6;     %m^2

%Coil Area
%------------
lcoil = 0.015;                      %mm
lusecoil = 0.005;                   %mm
dmcoile = 0.010;                    %mm
dtotalcoli = 1.256e-6;              %mm
umcoile= 2* pi * (dmcoile/2);       %mm Umfang
Acoil= pi * dmcoile^2;              %mm fläche


%Values
%---------------
roh= 0.017;          % Ω*mm/m Kupfer 20°C
mu0 = 0.000001256;    % 



%Diamater Kable
%----------------
d1 = 2*sqrt(Acable1/pi); %mm  
d2 = 2*sqrt(Acable2/pi); %mm
d3 = 2*sqrt(Acable3/pi); %mm
d4 = 2*sqrt(Acable4/pi); %mm


% Kable Numbers Length
%----------------------
nl1 = floor(lcoil/d1);  %N
nl2 = floor(lcoil/d2);  %N
nl3 = floor(lcoil/d3);  %N
nl4 = floor(lcoil/d4);  %N


% Kable Numbers wide
%----------------------
nw1 = floor(lusecoil/d1); %N   
nw2 = floor(lusecoil/d2); %N
nw3 = floor(lusecoil/d3); %N
nw4 = floor(lusecoil/d4); %N

%Numbers of Turnes
%--------------------
turnsN1 = floor(nl1*nw1);    %turns
turnsN2 = floor(nl2*nw2);    %turns
turnsN3 = floor(nl3*nw3);    %turns
turnsN4 = floor(nl4*nw4);    %turns


%Length wire
%--------------------
l1 = umcoile*turnsN1; %m diameter*turnes 
l2 = umcoile*turnsN2; %m
l3 = umcoile*turnsN3; %m
l4 = umcoile*turnsN4; %m


% Ω per wire
%--------------------
R1 = roh*l1/Acable1;         %Ω    
R2 = roh*l2/Acable2;         %Ω
R3 = roh*l3/Acable3;         %Ω
R4 = roh*l4/Acable4;         %Ω

% mH per wire
%--------------------
L1 = (Acoil*mu0*turnsN1^2/lcoil)*1000;   %mH
L2 = (Acoil*mu0*turnsN2^2/lcoil)*1000;   %mH
L3 = (Acoil*mu0*turnsN3^2/lcoil)*1000;   %mH
L4 = (Acoil*mu0*turnsN4^2/lcoil)*1000;   %mH

%step 1
%number of turns N
%Colil crossection Acloil
% risistens R
%induktans L
%µ0 mu0
%length l

% überarbeiten mm^2 
%110 0.318mH
%t=L/R
%L= N^2*Acloil/lcoul


%Step 2
%presantation formular
%2-3 for calculation
%2-3 for sensors ( buy, type, parameters)


%Step 3
%fmnk38
%git.thm. creat a project


%Step 4 
% list of sensors 
% check 5-10 possible sensors foto and dater


%Step 5
% maker space info kallina

%Step 6
% freCat basci cours youtube 
% eigenes model


