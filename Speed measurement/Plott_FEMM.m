
%Get data from FEEM in Arrya
%FEEM_data= csvread('MarbleMove-CurrentFix.csv'); %20A

%FEEM_data= csvread('MarbleMove-CurrentFix_4A.csv');
FEEM_data= csvread('MarbleMove-CurrentFix_5A.csv');
%FEEM_data= csvread('MarbleMove-CurrentFix_6A.csv');
%FEEM_data= csvread('MarbleMove-CurrentFix_7A.csv');

% %% Form for pold
 I_Coil1 = FEEM_data (:, 1);        %Curend Coil 1 in A
 I_Coil2 = FEEM_data (:, 2);        %Curend Coil 2 in A
 Possion = FEEM_data (:, 3)./1000;  %Possion in m
 Force   = FEEM_data (:, 4);        %Force Coil 1 & Coil 2
    
%Calculation: Sum of the Force
 Forcesum = trapz(Possion,Force);

%plot 
ylabel('Poss/m');
xlabel('I coil/ A');
zlabel('Magnetic Fielde/N');
tiledlayout(2,1)

ax1 = nexttile;
plot (Possion,Force,'LineWidth',2)
legend ('Magnetic Field/N',"FontSize", 15);
title('FEMM Simulation Possition and Force',"FontSize", 15)
xlabel("Poss/ m",'FontSize', 15);
ylabel('Magnetic Fild/N','FontSize', 15)
grid on


ax2 = nexttile;
plot (Possion,I_Coil2,Possion,I_Coil1,'LineWidth',2)
legend ('Curend Coil 1','Curend Coil 2',"FontSize", 15);
title(ax2,'FEMM Simulation Possion and Currend',"FontSize", 15)
xlabel("Poss/ m",'FontSize', 15);
ylabel('Current I/A','FontSize', 15)
grid on






