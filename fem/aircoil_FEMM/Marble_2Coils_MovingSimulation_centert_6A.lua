file = openfile("MarbleMove-CurrentFix_6A.csv", "w")

coil_length = 15 -- Length of coil
Ncoil = 300 -- Number of turns of coil
airgap = 6
coil_1_centre = -11
coil_2_centre = coil_1_centre + airgap + coil_length



for marblezpos = -40, 40, 1 do


if marblezpos < coil_1_centre then
    Icoil_1 = 0
    Icoil_2 = 6
elseif marblezpos >= coil_1_centre and marblezpos < coil_2_centre then
    Icoil_1 = 6
    Icoil_2 = 0
elseif marblezpos >= coil_2_centre then
    Icoil_1 = 0
    Icoil_2 = 0
end


newdocument(0)
mi_probdef(0,"millimeters","axi",1e-008)

--construct coil 1
mi_addnode(8,3)
mi_addnode(8,18)
mi_addnode(13,3)
mi_addnode(13,18)
mi_addsegment(8,3, 8, 18)
mi_addsegment(8,3, 13, 3)
mi_addsegment(8,18, 13, 18)
mi_addsegment(13,18, 13, 3)

--construct coil 2
mi_addnode(8,-3)
mi_addnode(8,-18)
mi_addnode(13,-3)
mi_addnode(13,-18)
mi_addsegment(8,-3, 8, -18)
mi_addsegment(8,-3, 13, -3)
mi_addsegment(8,-18, 13, -18)
mi_addsegment(13,-18, 13, -3)

--construct marble
mi_addnode(0,marblezpos+6.35)	
mi_addnode(0,marblezpos-6.35)
mi_addarc(0,marblezpos-6.35,0,marblezpos+6.35,180,1)

--construct boundary
mi_addnode(0,150)
mi_addnode(0,-150)
mi_addsegment(0,-150,0,150)
mi_addarc(0,-150,0,150,180,1)

--Load materials
mi_getmaterial('Air')	
mi_getmaterial('Copper')
mi_getmaterial('Steel castings, as cast');

am = 1 --Automesh on
ms = 10 --Mesh size (optional, only if automesh off)

-- Assign material and current to coil 1 
mi_addblocklabel(10,8)	
mi_selectlabel(10,8)	


mi_addcircprop('circuit_1',Icoil_1, 1) -- "1" means series connection
mi_setblockprop('Copper',am,ms,'circuit_1',90,0,Ncoil)    
mi_clearselected()

--Assign material and current to coil 2
mi_addblocklabel(10,-8)	
mi_selectlabel(10,-8)	

mi_addcircprop('circuit_2', Icoil_2, 1) -- "1" means series connection
mi_setblockprop('Copper',am,ms,'circuit_2',90,0,Ncoil)    
mi_clearselected()

-- Assign material to marble
mi_addblocklabel(1,marblezpos)	
mi_selectlabel(1,marblezpos)
mi_setblockprop('Steel castings, as cast',am,ms,'',0,0,0)    
mi_clearselected()

-- Air
mi_addblocklabel(20,0)
mi_selectlabel(20,0)
mi_setblockprop('Air',am,ms,'',0,0,0)
mi_clearselected()

--boundary (field lines parallel to border)
mi_selectarcsegment(1,150)
mi_addboundprop("a0", 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0)
mi_setarcsegmentprop(1, "a0", 0, 0)
mi_clearselected()

--save file
mi_saveas("aircoil.fem")

--solve and load
mi_analyze(0)
mi_loadsolution()

--calculate Force	
mo_selectblock(1,marblezpos)
Fy = mo_blockintegral(19)
mo_clearblock()


--print(Icoil_1 .. ", " .. marblezpos .. ", " .. Fy)

--str = Icoil_1 .. ", ".. marblezpos .. ", " .. Fy 
str = Icoil_1 .. ", " .. Icoil_2 .. ", " .. marblezpos .. ", " .. Fy

write(file,str.."\n")


mi_close()  --close preprocessor
mo_close() --close postprocessor



end

closefile(file)